package org.veerle.model.jaxb;

import java.util.Optional;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.veerle.model.ClinicalStudyParser;
import org.veerle.model.jaxb.ClinicalStudy;

/**
 * 
 * Tests for {@link ClinicalStudyParser}
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ClinicalStudyParserTest {

    @Test(description = "the loadModel method loads properly the example clinical study '/ClinicalStudy/TEST.xml'.")
    public void loadModel01() {
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/TEST.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        String criteria = clinicalStudy.get().getEligibility().getCriteria().getTextblock().trim();
        Assert.assertEquals(criteria, "Contents Of Criteria");
        Assert.assertNotNull(clinicalStudy.get().getIdInfo());
        Assert.assertEquals(clinicalStudy.get().getIdInfo().getNctId(), "NCT00000738");
    }

}
