package org.veerle.nlp;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import junit.framework.Assert;

import org.testng.annotations.Test;
import org.veerle.model.ClinicalStudyParser;
import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.EcogScore;
import org.veerle.model.jaxb.ClinicalStudy;
import org.veerle.nlp.CriteriaParser;
import org.veerle.nlp.ExplicitEcogParser;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * 
 * Tests for {@link ExplicitEcogParser}
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ExplicitEcogParserTest {

    @Test(description = "Test a basic negation for explicit ecog text")
    public void detectExplicitEcog01() {
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> tokenizedText =
            criteriaParser
                .tokenizeText("Patients with Eastern cooperative Oncology Group (ECOG) performance status > 2 will be excluded");
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(tokenizedText);
        Set<EcogScore> results = ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(
            Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE, EcogScore.TWO), results);

    }

    @Test(description = "Test a basic affirmation for explicit ecog text")
    public void detectExplicitEcog02() {
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> tokenizedText =
            criteriaParser
                .tokenizeText("Patients with Eastern cooperative Oncology Group (ECOG) performance status > 2 are recommended");
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(tokenizedText);
        Set<EcogScore> results = ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(Sets.newHashSet(EcogScore.THREE, EcogScore.FOUR), results);

    }

    @Test(description = "Test the explicit ECOG classification from NCT00152867 should be properly detected")
    public void detectExplicitEcog03() {
        // NCT00152867 0-2
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT00152867.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(optionalCriteria.get().getInclusionCriteria());
        Assert.assertEquals(
            Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE, EcogScore.TWO), results);

    }

    @Test(description = "Test the explicit ECOG classification from NCT00263822 should be properly detected")
    public void detectExplicitEcog04() {
        // NCT00263822 0-1
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT00263822.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(optionalCriteria.get().getInclusionCriteria());
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE), results);

    }

    @Test(description = "Test the explicit ECOG classification from NCT01382407 should be properly detected")
    public void detectExplicitEcog05() {
        // NCT01382407 0-3
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT01382407.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(optionalCriteria.get().getInclusionCriteria());
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE,
            EcogScore.TWO, EcogScore.THREE), results);
    }

    @Test(description = "Test the explicit ECOG classification from NCT01528488 should be properly detected")
    public void detectExplicitEcog06() {
        // NCT01528488 0-2
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT01528488.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Set<EcogScore> results =
            ecogDetector.detectExplicitEcog(optionalCriteria.get());
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE,
            EcogScore.TWO), results);
    }

    @Test(description = "Test the explicit ECOG classification from NCT01573442 should be properly detected")
    public void detectExplicitEcog07() {
        // NCT01573442 0-2
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT01573442.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Set<EcogScore> results =
            ecogDetector.detectExplicitEcog(optionalCriteria.get());
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE,
            EcogScore.TWO), results);
    }

    @Test(description = "Test the explicit ECOG classification from NCT01614938 should be properly detected")
    public void detectExplicitEcog08() {
        // NCT01614938 0-1
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT01614938.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Set<EcogScore> results =
            ecogDetector.detectExplicitEcog(optionalCriteria.get());
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE), results);
    }

    @Test(description = "Test the explicit ECOG classification from NCT01668498 should be properly detected")
    public void detectExplicitEcog09() {
        // NCT01668498 0-2
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT01668498.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Set<EcogScore> results =
            ecogDetector.detectExplicitEcog(optionalCriteria.get());
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE, EcogScore.TWO), results);
    }
    
    @Test(description = "Test the explicit ECOG classification from the query 'ECOG scores that are higher than 2' should be properly detected")
    public void detectExplicitEcog10() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> queryTokenized = criteriaParser.tokenizeText("ECOG scores that are higher than 2");
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(queryTokenized);
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(Sets.newHashSet(EcogScore.THREE, EcogScore.FOUR), results);
    }
    
    @Test(description = "Test the explicit ECOG classification from the query 'ECOG scores that are not higher than 2' should be properly detected")
    public void detectExplicitEcog11() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> queryTokenized = criteriaParser.tokenizeText("ECOG scores that are not higher than 2");
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(queryTokenized);
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE, EcogScore.TWO), results);
    }
    
    
    @Test(description = "Test the explicit ECOG classification from the query 'ECOG scores that are between 1 and 2' should be properly detected")
    public void detectExplicitEcog12() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> queryTokenized = criteriaParser.tokenizeText("ECOG scores that are between 1 and 2");
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(queryTokenized);
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(Sets.newHashSet(EcogScore.ONE, EcogScore.TWO), results);
    }

    @Test(description = "Test the explicit ECOG classification from the query 'ECOG scores that are lower or equal than 2' should be properly detected")
    public void detectExplicitEcog13() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> queryTokenized = criteriaParser.tokenizeText("ECOG scores that are lower or equal than 2");
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(queryTokenized);
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE, EcogScore.TWO), results);
    }
    
    @Test(description = "Test the explicit ECOG classification from the query 'ECOG scores that are not above 3' should be properly detected")
    public void detectExplicitEcog14() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> queryTokenized = criteriaParser.tokenizeText("ECOG scores that are not above 3");
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(queryTokenized);
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE, EcogScore.TWO, EcogScore.THREE), results);
    }
    
    
    @Test(description = "Test the explicit ECOG classification from the query 'ECOG scores above 3 are discarded' should be properly detected")
    public void detectExplicitEcog15() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> queryTokenized = criteriaParser.tokenizeText("ECOG scores above 3 are discarded");
        ExplicitEcogParser ecogDetector = new ExplicitEcogParser();
        Collection<Collection<String>> tokenizedTexts = Lists.newArrayList();
        tokenizedTexts.add(queryTokenized);
        Set<EcogScore> results =
            ecogDetector.parseExplicitEcogScores(tokenizedTexts);
        Assert.assertEquals(Sets.newHashSet(EcogScore.ZERO, EcogScore.ONE, EcogScore.TWO, EcogScore.THREE), results);
    }
    
}
