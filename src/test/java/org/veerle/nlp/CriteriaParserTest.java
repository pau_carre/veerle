package org.veerle.nlp;

import java.util.Collection;
import java.util.Optional;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.veerle.model.ClinicalStudyParser;
import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.EcogScore;
import org.veerle.model.jaxb.ClinicalStudy;
import org.veerle.nlp.CriteriaParser;

import com.google.common.collect.Lists;

/**
 * 
 * Tests for {@link CriteriaParser}
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class CriteriaParserTest {
    
    @Test(description="tokenizeText is able to parse properly the description for EcogPerformanceStatus.TWO")
    public void tokenizeText01(){
        Collection<String> expectedTwoGrade = Lists.newArrayList("ambulatory", "capable", "all", "self", "care", 
            "but", "unable", "carry", "out", "any", "work", "activitie", "up", "about", "more", "than", "50", "%", "wak", "hour");
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> twoGrade = criteriaParser.tokenizeText(EcogScore.TWO.getDescription());
        Assert.assertEquals(twoGrade, expectedTwoGrade);
    }

    @SuppressWarnings({"unchecked"})
    @Test(description="parseCriteria is able to parse properly the clinical study '/ClinicalStudy/NCT00095212.xml'")
    public void parseCriteria01(){
        
        Collection<Collection<String>> expectedInclusionCriteria =
            Lists.newArrayList(
                Lists.newArrayList("female","18", "55"), 
                Lists.newArrayList("bmi", "les", "than", "or", "equal", "26"), 
                Lists.newArrayList("hiv", "infect"), 
                Lists.newArrayList("androgen", "deficient", "free", "testosterone", "<", "3", "pg", "ml"),
                Lists.newArrayList("stable", "antiretroviral", "regiman", "3", "month", "prior", "study"),
                Lists.newArrayList("tubal", "ligation", "hysterectomy", "or", "verbaliz", "understand", "appropriate", 
                    "barri", "contraception", "methods.", "subject", "will", "be", "counsel", "appropriate", "barri", 
                    "contraception", "method", "counsel", "will", "be", "document")
                );

        Collection<Collection<String>> expectedExclusionCriteria =
            Lists.newArrayList(
                Lists.newArrayList("use", "anabolic", "agent", "includ", "testosterone", "gh", "or", "oth", "preparation", "within", "3", "month", "study"),
                Lists.newArrayList("use", "megestrol", "acetate", "within", "3", "month", "study"),
                Lists.newArrayList("use", "estrogen", "or", "any", "preparation", "known", "affect", "bone", "density", "or", "bone", "turnov", 
                    "thi", "include", "oral", "contraceptif", "depo", "provera", "or", "combin", "progesterone", "estrogen", "injection", "transdermal", "contraceptive", "patch"),
                Lists.newArrayList("pregnant", "or", "breast", "feed"),
                Lists.newArrayList("hgb", "<", "9.0", "mg", "dl"),
                Lists.newArrayList("current", "participation", "anoth", "research", "study", "conduct", "thi", "investigator", "or", "past",
                    "participation", "dhea", "study", "fund", "same", "grant", "as", "thi", "protocol"),
                Lists.newArrayList("creatinine", ">", "1.5", "mg", "dl")
                );
        
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy("/ClinicalStudy/NCT00095212.xml");
        Assert.assertTrue(clinicalStudy.isPresent());
        Assert.assertEquals(clinicalStudy.get().getIdInfo().getNctId(), "NCT00095212");
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria = criteriaParser.parseCriteria(clinicalStudy.get());
        Assert.assertTrue(optionalCriteria.isPresent());
        Assert.assertEquals(optionalCriteria.get().getInclusionCriteria(), expectedInclusionCriteria);
        Assert.assertEquals(optionalCriteria.get().getExclusionCriteria(), expectedExclusionCriteria);
    }
  
}
