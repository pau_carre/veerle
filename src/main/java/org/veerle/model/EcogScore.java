package org.veerle.model;

import java.util.Collection;
import java.util.Set;

import org.veerle.nlp.CriteriaParser;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Model of the ECOG scores. The fifth score is not modeled as it's not applicable
 * to any Clinical Announcement Trial.
 * 
 * The class also has additional methods to generate semantic 
 * information for Lucene indexing. This feature is no used in the current version.
 *  
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public enum EcogScore {

    ZERO("Fully active, able to carry on all pre-disease performance without restriction", 0), 
    ONE("Restricted in physically strenuous activity but ambulatory able to carry out work of a light or sedentary nature, e.g. light house work, office work", 1), 
    TWO("Ambulatory and capable of all self care but unable to carry out any work activities; up and about more than 50% of waking hours", 2), 
    THREE("Capable of only limited self care; confined to bed or chair more than 50% of waking hours", 3), 
    FOUR("Completely disabled; cannot carry on any self care; totally confined to bed or chair", 4);

    private String description;
    private Integer index;

    private EcogScore(String description, Integer index) {
        this.description = description;
        this.index = index;
    }

    public String getDescription() {
        return description;
    }

    public Integer getIndex() {
        return index;
    }
    
    public String getNormalizedDescription() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> tokenizedDescription = criteriaParser.tokenizeText(description);
        String description = Joiner.on(" ").join(tokenizedDescription);
        return description;
    }

    public String getNormalizedEnrichedDescription() {
        CriteriaParser criteriaParser = new CriteriaParser();
        Collection<String> tokenizedDescription = criteriaParser.tokenizeText(description);
        String description = Joiner.on(" ").join(tokenizedDescription);
        String comparisons = getComparisons();
        return description + ". " + comparisons;
    }

    private String getComparisons() {
        Set<String> ecogStrings = Sets.newHashSet("ECOG", "Eastern cooperative Oncology Group");
        Collection<String> comparisons = Lists.newArrayList();
        // >= comparisons
        for (int currentIndex = 0; currentIndex <= index; currentIndex++) {
            for (String ecog : ecogStrings) {
                comparisons.add(ecog + " " + currentIndex + "-" + index);
                if (index == currentIndex) {
                    for (String comparison : Sets.newHashSet(">=", "≥", "greater or equal than")) {
                        comparisons.add(ecog + " " + comparison + " " + currentIndex);
                    }
                } else {
                    for (String comparison : Sets.newHashSet(">", "greater than", "≥", ">=", "greater or equal than")) {
                        comparisons.add(ecog + " " + comparison + " " + currentIndex);
                    } 
                }
            }
        }
        // <= comparisons
        for (int currentIndex = index; currentIndex <= 5; currentIndex++) {
            for (String ecog : ecogStrings) {
                if (index == currentIndex) {
                    for (String comparison : Sets.newHashSet("<=", "≤", "less or equal than")) {
                        comparisons.add(ecog + " " + comparison + " " + currentIndex);
                    }
                } else {
                    for (String comparison : Sets.newHashSet("<", "less than", "<=", "≤", "less or equal than")) {
                        comparisons.add(ecog + " " + comparison + " " + currentIndex);
                    } 
                }
            }
        }
        // = comparisons
        for (String ecog : ecogStrings) {
            comparisons.add(ecog + " " + index);
            comparisons.add(ecog + " = " + index);
            comparisons.add(ecog + " equal to " + index);
        }
        return Joiner.on(". ").join(comparisons);
    }

    public static EcogScore fromIndex(Integer indexToFind) {
        for(EcogScore ecogPerformanceStatus : EcogScore.values()){
            if(ecogPerformanceStatus.getIndex() == indexToFind){
                return ecogPerformanceStatus;
            }
        }
        return null;
    }
}
