package org.veerle.model;

import java.util.HashSet;
import java.util.Set;
import java.util.Collection;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.stream.IntStream;
import java.util.stream.Collectors;;

/**
 * ECOG score comparison type.
 * It models the different ways either the user's queries or the CTAs
 * can provide a list of ECOG scores using comparisons of ECOG scores.
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public enum EcogComparisonType {
    
    LESS_THAN(Sets.newHashSet("<", "less than", "low than", "below"), 
        (Integer element) -> {
            return IntStream.rangeClosed(EcogScore.ZERO.getIndex(), element - 1).boxed().collect(Collectors.toSet());
        }), 
    EQUALS(Sets.newHashSet("=", "is"),
        (Integer element) -> {
            return Sets.newHashSet(element);
        }), 
    LESS_OR_EQUALS_THAN(Sets.newHashSet("<=", "=<", "≤", "low or equal", "less or equal"),
        (Integer element) -> {
            return IntStream.rangeClosed(EcogScore.ZERO.getIndex(), element).boxed().collect(Collectors.toSet());
        }), 
    MORE_OR_EQUALS_THAN(Sets.newHashSet(">=", "=>", "≥",  "high or equal than", "more or equal"),
        (Integer element) -> {
            return IntStream.rangeClosed(element, EcogScore.FOUR.getIndex()).boxed().collect(Collectors.toSet());
        }), 
    MORE_THAN(Sets.newHashSet(">", "more than", "big than", "high than", "above"),
        (Integer element) -> {
            return IntStream.rangeClosed(element + 1, EcogScore.FOUR.getIndex()).boxed().collect(Collectors.toSet());
        }), 
    BETWEEN(Sets.newHashSet(" ", "between"),
        (Integer element) -> {
            // not applicable -> return empty set
            return new HashSet<Integer>();
        });

    private Set<String> compatibleTexts;
    private Function<Integer, Set<Integer>> containedNumbersFunction;

    public static Collection<EcogComparisonType> getEcogComparisonTypeWithPreferenceOrder(){
        return Lists.newArrayList(LESS_OR_EQUALS_THAN, MORE_OR_EQUALS_THAN, MORE_THAN, LESS_THAN, BETWEEN, EQUALS);
    }
    
    private EcogComparisonType(Set<String> compatibleTexts, Function<Integer, Set<Integer>> isContainedFunction) {
        this.compatibleTexts = compatibleTexts;
        this.containedNumbersFunction = isContainedFunction;
    }

    public Set<String> getCompatibleTexts() {
        return compatibleTexts;
    }

    public Function<Integer, Set<Integer>> getContainedNumbersFunction() {
        return containedNumbersFunction;
    }
    
    

}
