package org.veerle.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.veerle.model.jaxb.ClinicalStudy;


/**
 * 
 * JAXB Parser for CTAs from Clinic Trials ( http://clinicaltrials.gov/ )
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ClinicalStudyParser {

	private static final Logger logger = LogManager.getLogger(ClinicalStudyParser.class);
	
	public Optional<ClinicalStudy> loadClinicalStudy(String resourceName) {
		try(InputStream resourceAsStream = ClinicalStudy.class.getResourceAsStream(resourceName)){
		    if(resourceAsStream == null){
		        logger.error("Resource "+ resourceName +" not found.");
		        return Optional.empty();
		    }
			JAXBContext jaxbContext = JAXBContext.newInstance(ClinicalStudy.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			ClinicalStudy clinicalStudy = (ClinicalStudy) unmarshaller
					.unmarshal(resourceAsStream);
			return Optional.of(clinicalStudy);
		} catch(IOException | JAXBException e) {
			logger.error("Error loading model from " + resourceName, e);
		}
		return Optional.empty();
	}

    public Optional<ClinicalStudy> loadClinicalStudyFromFile(String filename) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ClinicalStudy.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            ClinicalStudy clinicalStudy = (ClinicalStudy) unmarshaller
                    .unmarshal(new File(filename));
            return Optional.of(clinicalStudy);
        } catch(JAXBException e) {
            logger.error("Error loading model from the file" + filename, e);
        }
        return Optional.empty();
    }

}
