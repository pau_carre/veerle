package org.veerle.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;

/**
 * Clinical Trial Announcements which contain:
 * <ul>
 * <li>The CTA identifier/name</li>
 * <li>The collection of paragraphs of the inclusion criteria</li>
 * <li>The collection of paragraphs of the exclusion criteria</li>
 * </ul>
 * 
 * The class also has additional methods to generate semantic 
 * information for Lucene indexing. This feature is no used in the current version.
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ClinicalTrialAnnouncement implements Serializable {

    private static final long serialVersionUID = -7276586215412794263L;

    private String id;
    private Collection<Collection<String>> inclusionCriteria;
    private Collection<Collection<String>> exclusionCriteria;

    public ClinicalTrialAnnouncement(String id, Collection<Collection<String>> inclusionCriteria,
        Collection<Collection<String>> exclusionCriteria) {
        super();
        this.id = id;
        this.inclusionCriteria = inclusionCriteria;
        this.exclusionCriteria = exclusionCriteria;
    }

    public ClinicalTrialAnnouncement(String id) {
        super();
        this.id = id;
        this.inclusionCriteria = new ArrayList<Collection<String>>();
        this.exclusionCriteria = new ArrayList<Collection<String>>();
    }

    public String getId() {
        return id;
    }

    public Collection<Collection<String>> getInclusionCriteria() {
        return inclusionCriteria;
    }

    public String getInclusionCriteriaAsText() {
        return doubleCollectionToString(inclusionCriteria, ". ", " ", Optional.empty());
    }

    public Collection<Collection<String>> getExclusionCriteria() {
        return exclusionCriteria;
    }

    public String getExclusionCriteriaAsText() {
        return doubleCollectionToString(exclusionCriteria, ". ", " ", Optional.empty());
    }

    private String doubleCollectionToString(Collection<Collection<String>> criterias, String externalConnector,
        String internalConnector, Optional<Function<String, String>> transformer) {
        StringBuffer stringBuffer = new StringBuffer();
        for (Iterator<Collection<String>> criteriaIterator = criterias.iterator(); criteriaIterator.hasNext();) {
            Collection<String> criteria = criteriaIterator.next();
            Collection<String> transformedCriteria =
                Collections2.transform(criteria, transformer.orElse((String element) -> element));
            stringBuffer.append(Joiner.on(internalConnector).join(transformedCriteria));
            if (criteriaIterator.hasNext()) {
                stringBuffer.append(externalConnector);
            }
        }
        return stringBuffer.toString();
    }

    @Override
    public String toString() {
        return "Criteria [inclusionCriteria=" + inclusionCriteria + ", exclusionCriteria=" + exclusionCriteria + "]";
    }

    public String getBooleanQuery() {
        String inclusion =
            "(" + doubleCollectionToString(inclusionCriteria, " ) OR ( ", " OR ", Optional.empty()) + ")";
        String exclusion =
            "(NOT(" + doubleCollectionToString(exclusionCriteria, " ) OR NOT( ", " OR ", Optional.empty()) + "))";
        return inclusion + (exclusionCriteria.size() > 0 ? " OR " + exclusion : "");
    }

    public String getTextualQuery() {
        String inclusion = "(" + doubleCollectionToString(inclusionCriteria, " ) OR ( ", " ", Optional.empty()) + ")";
        String exclusion = "(" + doubleCollectionToString(exclusionCriteria, " ) OR ( ", " ", Optional.empty()) + ")";
        return inclusion + (exclusionCriteria.size() > 0 ? " OR NOT (" + exclusion + ")" : "");
    }

}
