//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.05.26 at 10:48:37 AM IST 
//


package org.veerle.model.jaxb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.veerle package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.veerle
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClinicalStudy }
     * 
     */
    public ClinicalStudy createClinicalStudy() {
        return new ClinicalStudy();
    }

    /**
     * Create an instance of {@link IdInfoStruct }
     * 
     */
    public IdInfoStruct createIdInfoStruct() {
        return new IdInfoStruct();
    }

    /**
     * Create an instance of {@link EligibilityStruct }
     * 
     */
    public EligibilityStruct createEligibilityStruct() {
        return new EligibilityStruct();
    }

    /**
     * Create an instance of {@link TextblockStruct }
     * 
     */
    public TextblockStruct createTextblockStruct() {
        return new TextblockStruct();
    }

}
