package org.veerle.nlp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.ctakes.clinicalpipeline.ClinicalPipelineFactory;
import org.apache.ctakes.typesystem.type.syntax.ContractionToken;
import org.apache.ctakes.typesystem.type.syntax.NumToken;
import org.apache.ctakes.typesystem.type.syntax.PunctuationToken;
import org.apache.ctakes.typesystem.type.syntax.SymbolToken;
import org.apache.ctakes.typesystem.type.syntax.WordToken;
import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.tcas.Annotation;
import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.jaxb.ClinicalStudy;

import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * Tokenizes+normalizes queries and parses
 * {@link ClinicalTrialAnnouncement} from {@link ClinicalStudy}s
 *
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 */
public class CriteriaParser {

    private static final Logger logger = LogManager.getLogger(CriteriaParser.class);
    private static AnalysisEngineDescription analysisEngineDescription;

    public CriteriaParser() {

    }

    public Collection<String> tokenizeText(String text) {
        try {
            Collection<Annotation> annotations = getAnnotations(text);
            Collection<String> nullableAnnotationsText =
                Collections2.transform(annotations, (Annotation annotation) -> getStringRepresentation(annotation)
                    .orElse((String) null));
            Collection<String> annotationsText =
                Collections2.filter(nullableAnnotationsText, (String annotation) -> !Strings.isNullOrEmpty(annotation));
            return annotationsText;
        } catch (UIMAException e) {
            logger.error("Error trying to tokenize the text '" + text + "'", e);
        }
        return Lists.newArrayList();
    }

    public Optional<ClinicalTrialAnnouncement> parseCriteria(ClinicalStudy clinicalStudy) {
        Optional<String> optionalText = clinicalStudy.accessCriteria();
        Optional<ClinicalTrialAnnouncement> optionalCriteria = Optional.empty();
        if (optionalText.isPresent()) {
            String text = optionalText.get();
            try {
                Collection<Annotation> annotations = getAnnotations(text);
                List<Integer> orderedStartOfParagraph = getStartsOfParagraph(annotations);
                Optional<Range<Integer>> inclusionCriteriaIndex = getInclusionCriteriaRange(annotations);
                Optional<Range<Integer>> exclusionCriteriaIndex = getExclusionCriteriaRange(annotations);
                if (!inclusionCriteriaIndex.isPresent() && !exclusionCriteriaIndex.isPresent()) {
                    return parseCriteria(clinicalStudy, annotations, orderedStartOfParagraph);
                }
                if (inclusionCriteriaIndex.isPresent() && exclusionCriteriaIndex.isPresent()
                    && inclusionCriteriaIndex.get().upperEndpoint() < exclusionCriteriaIndex.get().lowerEndpoint()) {
                    Range<Integer> inclusionCriteriaContentsRange =
                        Range.open(inclusionCriteriaIndex.get().upperEndpoint(), exclusionCriteriaIndex.get()
                            .lowerEndpoint());
                    Range<Integer> exclusionCriteriaContentsRange =
                        Range.open(exclusionCriteriaIndex.get().upperEndpoint(), text.length());

                    Iterator<Annotation> annotationIterator = annotations.iterator();
                    List<String> currentParagraph = new ArrayList<String>();
                    ClinicalTrialAnnouncement criteria = new ClinicalTrialAnnouncement(clinicalStudy.getIdInfo().getNctId());
                    Integer lastStartOfParagraph = -1;
                    Boolean inclusionCriteriaFound = false;
                    Boolean exclusionCriteriaFound = false;
                    while (annotationIterator.hasNext()) {
                        Annotation annotation = annotationIterator.next();
                        Optional<String> stringRepresentation = getStringRepresentation(annotation);
                        if (stringRepresentation.isPresent()) {
                            Boolean newParagraphFound =
                                orderedStartOfParagraph.size() > 0
                                    && annotation.getBegin() >= orderedStartOfParagraph.get(0)
                                    && orderedStartOfParagraph.get(0) > lastStartOfParagraph;
                            if (newParagraphFound) {
                                if (inclusionCriteriaContentsRange.contains(annotation.getBegin())) {
                                    if (inclusionCriteriaFound) {
                                        criteria.getInclusionCriteria().add(currentParagraph);
                                    } else {
                                        inclusionCriteriaFound = true;
                                    }
                                } else if (exclusionCriteriaContentsRange.contains(annotation.getBegin())) {
                                    if (!exclusionCriteriaFound) {
                                        criteria.getInclusionCriteria().add(currentParagraph);
                                        exclusionCriteriaFound = true;
                                    } else {
                                        criteria.getExclusionCriteria().add(currentParagraph);
                                    }
                                }
                                lastStartOfParagraph = orderedStartOfParagraph.get(0);
                                orderedStartOfParagraph.remove(0);
                                currentParagraph = new ArrayList<String>();
                            }
                            if (inclusionCriteriaContentsRange.contains(annotation.getBegin())
                                || exclusionCriteriaContentsRange.contains(annotation.getBegin())) {
                                currentParagraph.add(stringRepresentation.get());
                            }
                        }
                    }
                    if (!currentParagraph.isEmpty() && exclusionCriteriaFound) {
                        criteria.getExclusionCriteria().add(currentParagraph);
                    } else {
                        criteria.getInclusionCriteria().add(currentParagraph);
                    }
                    optionalCriteria = Optional.of(criteria);
                }
            } catch (UIMAException e) {
                logger.error("Error trying to parse the criteria from the text '" + text + "'", e);
            }
        }
        return optionalCriteria;
    }


    private Optional<ClinicalTrialAnnouncement> parseCriteria(ClinicalStudy clinicalStudy, Collection<Annotation> annotations,
        List<Integer> orderedStartOfParagraph) {
        Iterator<Annotation> annotationIterator = annotations.iterator();
        List<String> currentParagraph = new ArrayList<String>();
        ClinicalTrialAnnouncement criteria = new ClinicalTrialAnnouncement(clinicalStudy.getIdInfo().getNctId());
        Integer lastStartOfParagraph = -1;
        Boolean inclusionCriteriaFound = false;
        while (annotationIterator.hasNext()) {
            Annotation annotation = annotationIterator.next();
            Optional<String> stringRepresentation = getStringRepresentation(annotation);
            if (stringRepresentation.isPresent()) {
                Boolean newParagraphFound =
                    orderedStartOfParagraph.size() > 0 && annotation.getBegin() >= orderedStartOfParagraph.get(0)
                        && orderedStartOfParagraph.get(0) > lastStartOfParagraph;
                if (newParagraphFound) {
                    if (inclusionCriteriaFound) {
                        criteria.getInclusionCriteria().add(currentParagraph);
                    } else {
                        inclusionCriteriaFound = true;
                    }
                    lastStartOfParagraph = orderedStartOfParagraph.get(0);
                    orderedStartOfParagraph.remove(0);
                    currentParagraph = new ArrayList<String>();
                }
                currentParagraph.add(stringRepresentation.get());
            }
        }
        if (!currentParagraph.isEmpty()) {
            criteria.getExclusionCriteria().add(currentParagraph);
        } else {
            criteria.getInclusionCriteria().add(currentParagraph);
        }
        Optional<ClinicalTrialAnnouncement> optionalCriteria = Optional.of(criteria);
        return optionalCriteria;
    }

    private Optional<String> getStringRepresentation(Annotation annotation) {
        if (annotation instanceof WordToken) {
            return Optional.ofNullable(((WordToken) annotation).getCanonicalForm());
        } else if (annotation instanceof NumToken || annotation instanceof SymbolToken
            || annotation instanceof ContractionToken) {
            if (!Sets.newHashSet("*", "?", "^", "(", ")").contains(annotation.getCoveredText())) {
                return Optional.ofNullable(annotation.getCoveredText());
            }
        } else if (annotation instanceof PunctuationToken
            && Sets.newHashSet("=", "<", ">").contains(annotation.getCoveredText())) {
            return Optional.ofNullable(annotation.getCoveredText());
        }
        return Optional.empty();
    }

    private List<Integer> getStartsOfParagraph(Collection<Annotation> annotations) {
        Set<Integer> sentences =
            Sets.newHashSet(Collections2.transform(FluentIterable.from(annotations).filter(Sentence.class).toList(), (
                Sentence annotation) -> annotation.getBegin()));
        Set<Integer> scoreTokens =
            Sets.newHashSet(Collections2.transform(Collections2.filter(
                FluentIterable.from(annotations).filter(PunctuationToken.class).toList(), (
                    PunctuationToken punctuationToken) -> punctuationToken.getCoveredText().equals("-")), (
                PunctuationToken annotation) -> annotation.getBegin()));
        Set<Integer> numbersBeforeAdot =
            Sets.newHashSet(Collections2.transform(Collections2.filter(
                FluentIterable.from(annotations).filter(NumToken.class).toList(), (NumToken annotation) -> annotation
                    .getCoveredText().endsWith(".")), (NumToken annotation) -> annotation.getBegin()));

        Collection<Integer> startOfParagraph =
            Sets.newHashSet(Sets.intersection(Sets.union(scoreTokens, numbersBeforeAdot), sentences));
        List<Integer> orderedStartOfParagraph =
            Lists.newArrayList(Ordering.natural().immutableSortedCopy(startOfParagraph));
        return orderedStartOfParagraph;
    }

    private Collection<Annotation> getAnnotations(String text) throws UIMAException {
        JCas jcas = JCasFactory.createJCas();
        jcas.setDocumentText(text);
        synchronized (CriteriaParser.class) {
            if (analysisEngineDescription == null) {
                analysisEngineDescription = ClinicalPipelineFactory.getTokenProcessingPipeline();
            }
        }
        SimplePipeline.runPipeline(jcas, analysisEngineDescription);
        Collection<TOP> tops = JCasUtil.selectAll(jcas);
        Collection<Annotation> annotations = FluentIterable.from(tops).filter(Annotation.class).toList();
        return annotations;
    }


    private Optional<Range<Integer>> getInclusionCriteriaRange(Collection<Annotation> annotationIterator) {
        List<String> inclusionCriteria = Lists.newArrayList("inclusion", "criteria");
        return getRangeForCollectionOfStrings(annotationIterator, inclusionCriteria);
    }

    private Optional<Range<Integer>> getExclusionCriteriaRange(Collection<Annotation> annotationIterator) {
        List<String> inclusionCriteria = Lists.newArrayList("exclusion", "criteria");
        return getRangeForCollectionOfStrings(annotationIterator, inclusionCriteria);
    }

    private Optional<Range<Integer>> getRangeForCollectionOfStrings(Collection<Annotation> annotations,
        List<String> criteria) {
        Iterator<Annotation> annotationIterator = annotations.iterator();
        Integer lower = null;
        while (annotationIterator.hasNext() && !criteria.isEmpty()) {
            Annotation annotation = annotationIterator.next();
            Optional<String> stringRepresentation = getStringRepresentation(annotation);
            if (stringRepresentation.isPresent() && criteria.get(0).equals(stringRepresentation.get())) {
                if (lower == null) {
                    lower = annotation.getBegin();
                }
                criteria.remove(stringRepresentation.get());
            }
            if (criteria.isEmpty()) {
                return Optional.of(Range.closed(lower, annotation.getEnd()));
            }
        }
        return Optional.empty();
    }

}
