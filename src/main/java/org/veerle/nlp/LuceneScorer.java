package org.veerle.nlp;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.util.Version;
import org.veerle.model.ClinicalStudyParser;
import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.EcogScore;
import org.veerle.model.jaxb.ClinicalStudy;
/**
 * It extract the scores for a {@link ClinicalStudy} ( {@link ClinicalTrialAnnouncement} ) 
 * for each ECOG scores using Lucene.
 * 
 * NOTE: this feature is not used for this version
 *
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 */
public class LuceneScorer {

    public Map<EcogScore, Float> scoreECOG(String clinicalStudyResource) throws ParseException, IOException {
        Map<EcogScore, Float> results = new LinkedHashMap<EcogScore, Float>();
        LuceneIndexer luceneIndexer = new LuceneIndexer();
        IndexReader reader = luceneIndexer.getIndexReader();
        IndexSearcher searcher = new IndexSearcher(reader);
        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
        QueryParser parser = new QueryParser(Version.LUCENE_40, LuceneIndexer.ECOG_ENRICHED_DESCRIPTION, analyzer);
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudy(clinicalStudyResource);
        if (clinicalStudy.isPresent() && clinicalStudy.get().accessCriteria().isPresent()) {
            CriteriaParser criteriaParser = new CriteriaParser();
            Optional<ClinicalTrialAnnouncement> optionalCriteria =
                criteriaParser.parseCriteria(clinicalStudy.get());
            if (optionalCriteria.isPresent()) {
                Query q = parser.parse(optionalCriteria.get().getBooleanQuery());
                TopScoreDocCollector collector = TopScoreDocCollector.create(5, true);
                searcher.search(q, collector);
                ScoreDoc[] hits = collector.topDocs().scoreDocs;
                for (ScoreDoc scoreDoc : hits) {
                    Document doc = reader.document(scoreDoc.doc);
                    String name = doc.get(LuceneIndexer.NAME);
                    results.put(EcogScore.valueOf(name), scoreDoc.score);
                }
            }
        }
        reader.close();
        return results;
    }
}
