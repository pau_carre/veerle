package org.veerle.nlp;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.EcogComparisonType;
import org.veerle.model.EcogScore;

import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import com.google.common.collect.Ordering;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;

/**
 * It finds the ECOG scores from user's queries and {@link ClinicalTrialAnnouncement}s
 *
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 */
public class ExplicitEcogParser {

    public Set<EcogScore> detectExplicitEcog(ClinicalTrialAnnouncement criteria) {
        Set<EcogScore> ecogsIncluded = parseExplicitEcogScores(criteria.getInclusionCriteria());
        Set<EcogScore> ecogsExcluded = parseExplicitEcogScores(criteria.getExclusionCriteria());
        if (ecogsExcluded.isEmpty()) {
            return ecogsIncluded;
        } else {
            return Sets.union(ecogsIncluded,
                Sets.difference(Sets.newHashSet(EcogScore.values()), ecogsExcluded));
        }

    }

    public Set<EcogScore> parseExplicitEcogScores(Collection<Collection<String>> inclusionCriteria) {
        // get ECOG related line
        Optional<Collection<String>> ecogRelatedLine = getEcogRelatedLine(inclusionCriteria);
        if (ecogRelatedLine.isPresent()) {
            // detect comparison
            Set<Integer> ecogNumbers = getEcogIndexes(ecogRelatedLine.get());
            return Sets.newHashSet(Collections2.transform(ecogNumbers,
                (Integer index) -> EcogScore.fromIndex(index)));
        }
        return new HashSet<EcogScore>();
    }

    private static Set<String> negativeTokens = null;

    private Set<String> getNegativeTokens() {
        if (negativeTokens == null) {
            String negativeWords =
                "abandon cancel dispose ditch not "
                    + "eliminate reject remove renounce repeal banish drop eject expel forsake oust "
                    + "ban block exclude excluded eliminate ignore skip omit prevent prohibit discard";
            CriteriaParser criteriaParser = new CriteriaParser();
            negativeTokens = Sets.newHashSet(criteriaParser.tokenizeText(negativeWords));
        }
        return negativeTokens;
    }

    private Set<Integer> getEcogIndexes(Collection<String> criteria) {
        String criteriaAsString = Joiner.on(" ").join(criteria);
        for (EcogComparisonType comparisonType : EcogComparisonType.getEcogComparisonTypeWithPreferenceOrder()) {
            for (String currentComparison : comparisonType.getCompatibleTexts()) {
                if (criteriaAsString.contains(currentComparison)) {
                    Boolean isNegated =
                        !Sets.intersection(getNegativeTokens(), Sets.newHashSet(criteria)).isEmpty();
                    if (comparisonType.equals(EcogComparisonType.BETWEEN)) {
                        Optional<Range<Integer>> rangeOfDigits = getRangeOfDigits(criteria);
                        if (rangeOfDigits.isPresent()) {
                            return getBetweenRange(rangeOfDigits.get().lowerEndpoint(), rangeOfDigits.get()
                                .upperEndpoint(), isNegated);

                        }
                    } else {
                        Pattern pattern = Pattern.compile(".*([0-5]).*");
                        Matcher matcher = pattern.matcher(criteriaAsString);
                        if (matcher.find() && matcher.groupCount() == 1) {
                            Integer number = Integer.parseInt(matcher.group(1));
                            Set<Integer> ecogScores = comparisonType.getContainedNumbersFunction().apply(number);
                            if (isNegated) {
                                return Sets.difference(Sets.newHashSet(0, 1, 2, 3, 4), ecogScores);
                            } else {
                                return ecogScores;
                            }
                        }
                    }
                }
            }
        }
        return new HashSet<Integer>();
    }

    private Optional<Range<Integer>> getRangeOfDigits(Collection<String> criteria) {
        Set<Integer> integersFound = new HashSet<Integer>();
        for (String currentWord : criteria) {
            if (Sets.newHashSet("0", "1", "2", "3", "4").contains(currentWord)) {
                integersFound.add(Integer.parseInt(currentWord));
            }
        }
        if (integersFound.size() >= 2) {
            return Optional.of(Range.closed(Ordering.<Integer>natural().min(integersFound), Ordering.<Integer>natural()
                .max(integersFound)));
        }
        return Optional.empty();
    }

    private Set<Integer> getBetweenRange(Integer leftSide, Integer rightSide, Boolean isNegated) {
        Integer min = leftSide > rightSide ? rightSide : leftSide;
        Integer max = leftSide < rightSide ? rightSide : leftSide;
        Set<Integer> ecogScores = new HashSet<Integer>();
        for (int i = min; i <= max; i++) {
            ecogScores.add(i);
        }
        if (isNegated) {
            return Sets.difference(Sets.newHashSet(0, 1, 2, 3, 4), ecogScores);
        } else {
            return ecogScores;
        }
    }

    private Optional<Collection<String>> getEcogRelatedLine(Collection<Collection<String>> criteria) {
        Set<String> ecogValues = Sets.newHashSet("european", "cooperative", "oncology", "group");
        String ecog = "ecog";
        for (Collection<String> inclusionCriteria : criteria) {
            if (Sets.newHashSet(inclusionCriteria).contains(ecog)) {
                return Optional.of(inclusionCriteria);
            }
            if (Sets.intersection(Sets.newHashSet(inclusionCriteria), ecogValues).size() > 2) {
                return Optional.of(inclusionCriteria);
            }
        }
        return Optional.empty();
    }

}
