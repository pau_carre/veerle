package org.veerle.nlp;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.veerle.model.EcogScore;

/**
 * It indexes ECOG scores as documents in Lucene so they could be indexable for queries.
 * NOTE: this feature is not used for this version
 *
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 */
public class LuceneIndexer {

    private static final String INDEX_FOLDER = "index";
    public static final String NAME = "NAME";
    public static final String INCLUSION = "INCLUSION";
    public static final String EXCLUSION = "EXCLUSION";
    public static final String ECOG_DESCRIPTION = "ECOG_DESCRIPTION";
    public static final String ECOG_ENRICHED_DESCRIPTION = "ECOG_ENRICHED_DESCRIPTION";

    public LuceneIndexer() {}

    private static IndexWriter indexWriter = null;

    private void buildIndexWriter() throws IOException {
        if (indexWriter == null) {
            Directory indexDirecotry = FSDirectory.open(new File(INDEX_FOLDER));
            IndexWriterConfig config =
                new IndexWriterConfig(Version.LUCENE_40, new StandardAnalyzer(Version.LUCENE_40));
            indexWriter = new IndexWriter(indexDirecotry, config);
        }
    }

    public IndexReader getIndexReader() throws IOException {
        Directory indexDirecotry = FSDirectory.open(new File(INDEX_FOLDER));
        IndexReader reader = DirectoryReader.open(indexDirecotry);
        return reader;
    }

    private void closeIndexWriter() throws IOException {
        if (indexWriter != null) {
            indexWriter.close();
        }
    }

    private void indexEcogPerformanceStatusNormalizedEnrichedDescription() throws IOException {
        for (EcogScore ecogPerformanceStatus : EcogScore.values()) {
            Document document = new Document();
            document.add(new StringField(NAME, ecogPerformanceStatus.name(), Field.Store.YES));
            String  ecogPerformanceStatusDoument = ecogPerformanceStatus.getNormalizedEnrichedDescription();
            document.add(new TextField(ECOG_ENRICHED_DESCRIPTION, ecogPerformanceStatusDoument, Field.Store.YES));
            indexWriter.addDocument(document);
        }
    }

    private void indexEcogPerformanceStatusNormalizedDescription() throws IOException {
        for (EcogScore ecogPerformanceStatus : EcogScore.values()) {
            Document document = new Document();
            document.add(new StringField(NAME, ecogPerformanceStatus.name(), Field.Store.YES));
            String  ecogPerformanceStatusDoument = ecogPerformanceStatus.getNormalizedDescription();
            document.add(new TextField(ECOG_DESCRIPTION, ecogPerformanceStatusDoument, Field.Store.YES));
            indexWriter.addDocument(document);
        }
    }

    public void buidIndexes() throws IOException {
        buildIndexWriter();
        indexEcogPerformanceStatusNormalizedEnrichedDescription();
        indexEcogPerformanceStatusNormalizedDescription();
        closeIndexWriter();
    }
}
