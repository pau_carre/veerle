package org.veerle.spark;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.EcogScore;
import org.veerle.model.jaxb.ClinicalStudy;

import scala.Tuple2;

import com.google.common.base.Strings;
import com.google.common.collect.Collections2;

/**
 * 
 * Spark work to create {@link EcogScore} indexes for each {@link ClinicalTrialAnnouncement}.
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ParallelEcogsIndexDatabaseCreator {

    public static final String VEERLE_HOME = "VEERLE_HOME";
    public static final String VEERLE_HOST = "VEERLE_HOST";
    
    private static final Logger logger = LogManager.getLogger(ParallelEcogsIndexDatabaseCreator.class);

    private String veerleHome;
    private String veerleHost;
    
    public static void main(String[] args) {
        String veerleHome = System.getenv().get(VEERLE_HOME);
        String veerleHost = System.getenv().get(VEERLE_HOST);
        if(Strings.isNullOrEmpty(veerleHome)){
            logger.error("Error VEERLE_HOME environment variable not found.");
        } else if(Strings.isNullOrEmpty(veerleHost)){
            logger.error("Error VEERLE_HOST environment variable not found.");
        } else {
            ParallelEcogsIndexDatabaseCreator veerle = new ParallelEcogsIndexDatabaseCreator(veerleHome, veerleHost);
            veerle.createEcogsIndex();
        }
    }
    
    public ParallelEcogsIndexDatabaseCreator(String veerleHome, String veerleHost){
        this.veerleHome = veerleHome;
        this.veerleHost = veerleHost;
    }

    @SuppressWarnings("resource")
    private void createEcogsIndex() {
        logger.info("Creating ECOG indexed database.");
        SparkConf conf =
            new SparkConf()
                .setAppName("Veerle")
                .setMaster("spark://"+veerleHost+":7077")
                .setSparkHome(veerleHome + File.separatorChar + "spark-1.3.1-bin-hadoop2.6")
                .set("spark.driver.host", veerleHost)
                .setJars(
                    new String[] { veerleHome  + File.separatorChar + "veerle" + 
                                   File.separatorChar + "target" + File.separatorChar + 
                                   "veerle-1.0.0-SNAPSHOT-jar-with-dependencies.jar"});
        JavaSparkContext javaSparkContext = new JavaSparkContext(conf);
        List<String> ctaFiles = getCtas();
        JavaRDD<String> rddCtaFiles = javaSparkContext.parallelize(ctaFiles);
        logger.info("Parsing CTA files: " + rddCtaFiles.collect());
        // parallel execution of tasks
        JavaRDD<ClinicalStudy> rddClinicalStudies = parallelClinicalStudyParser(rddCtaFiles);
        JavaRDD<ClinicalTrialAnnouncement> rddCtas = parallelClinicalTrialAnnouncementParser(rddClinicalStudies);
        Collection<Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>> ecogs =
            parallelClinicalTrialAnnouncementEcogScoreParser(rddCtas);
        Map<Integer, Collection<String>> indexedCtas = getIndexedCtas(ecogs);
        // save index
        String filename = veerleHome  + File.separatorChar + "ecogs.index";
        seralize(indexedCtas, filename);
        logger.info("ECOG indexed database successfully created in " + filename);
    }

    private Map<Integer, Collection<String>> getIndexedCtas(
        Collection<Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>> ecogs) {
        Map<Integer, Collection<String>> indexedCtas = new java.util.HashMap<Integer, Collection<String>>();
        for (EcogScore ecogScoreStatus : EcogScore.values()) {
            indexedCtas.put(ecogScoreStatus.getIndex(), new ArrayList<String>());
        }
        for (Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>> ctaToEcog : ecogs) {
            for (EcogScore ecogScoreStatus : ctaToEcog._2) {
                indexedCtas.get(ecogScoreStatus.getIndex()).add(ctaToEcog._1.getId());
            }
        }
        logger.info("ECOG indexes: " + indexedCtas);
        return indexedCtas;
    }

    private Collection<Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>> parallelClinicalTrialAnnouncementEcogScoreParser(
        JavaRDD<ClinicalTrialAnnouncement> rddCtas) {
        JavaRDD<Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>> rddEcogs =
            rddCtas.map(new ParallelClinicalTrialAnnouncementEcogScoreParserFunction());
        Collection<Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>> ctaToEcogs = rddEcogs.collect();
        ctaToEcogs =
            Collections2.filter(ctaToEcogs,
                (Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>> ctaToEcog) -> ctaToEcog != null);
        logger.info("Discovered ECOGs: " + ctaToEcogs);
        return ctaToEcogs;
    }

    private JavaRDD<ClinicalTrialAnnouncement> parallelClinicalTrialAnnouncementParser(
        JavaRDD<ClinicalStudy> rddClinicalStudies) {
        JavaRDD<ClinicalTrialAnnouncement> rddCtas =
            rddClinicalStudies.map(new ParallelClinicalTrialAnnouncementParserFunction());
        Collection<ClinicalTrialAnnouncement> clinicalTrialAnnouncements = rddCtas.collect();
        clinicalTrialAnnouncements =
            Collections2.filter(clinicalTrialAnnouncements,
                (ClinicalTrialAnnouncement clinicalTrialAnnouncement) -> clinicalTrialAnnouncement != null);
        logger.info("Criteria CTAs parserd: " + clinicalTrialAnnouncements);
        return rddCtas;
    }

    private JavaRDD<ClinicalStudy> parallelClinicalStudyParser(JavaRDD<String> rddCtaFiles) {
        JavaRDD<ClinicalStudy> rddClinicalStudies = rddCtaFiles.map(new ParallelClinicalStudyParserFunction());
        Collection<ClinicalStudy> clinicalStudies = rddClinicalStudies.collect();
        clinicalStudies = Collections2.filter(clinicalStudies, (ClinicalStudy clinicalStudy) -> clinicalStudy != null);
        Collection<String> clinicalStudiesNames =
            Collections2.transform(clinicalStudies, (ClinicalStudy clinicalStudy) -> clinicalStudy.getIdInfo()
                .getNctId());
        logger.info("XML CTAs parserd: " + clinicalStudiesNames);
        return rddClinicalStudies;
    }


    private List<String> getCtas() {
        List<String> ctaFiles = new LinkedList<String>();
        File clinicalStudyFolder = new File(veerleHome + File.separatorChar + "ClinicalStudy");
        File[] listOfFilesInClinicalStudy = clinicalStudyFolder.listFiles();
        for (int i = 0; i < listOfFilesInClinicalStudy.length; i++) {
            if (listOfFilesInClinicalStudy[i].isFile()) {
                ctaFiles.add(listOfFilesInClinicalStudy[i].getAbsolutePath());
            }
        }
        return ctaFiles;
    }

    private void seralize(Object object, String filename) {
        try {
            File file = new File(filename);
            if (!file.exists()) {
                file.createNewFile();
            } else {
                logger.info("File " + filename + " already exists. Recreating the file.");
                file.delete();
                file.createNewFile();
            }
            try (FileOutputStream fileOut = new FileOutputStream(file);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);) {
                out.writeObject(object);
            } catch (IOException e) {
                logger.error("Error selializing " + object, e);
            }
        } catch (IOException e) {
            logger.error("Error selializing " + object, e);
        }
    }

   

}
