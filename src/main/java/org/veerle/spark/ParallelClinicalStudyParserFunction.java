package org.veerle.spark;

import java.io.Serializable;
import java.util.Optional;

import org.apache.spark.api.java.function.Function;
import org.veerle.model.ClinicalStudyParser;
import org.veerle.model.jaxb.ClinicalStudy;


/**
 * 
 * Spark parallel {@link Function} to parse XML files into {@link ClinicalStudy}s
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ParallelClinicalStudyParserFunction implements Function<String, ClinicalStudy>, Serializable  {

    private static final long serialVersionUID = -8890489222735888795L;

    @Override
    public ClinicalStudy call(String filename) {
        ClinicalStudyParser clinicalStudyParser = new ClinicalStudyParser();
        Optional<ClinicalStudy> clinicalStudy = clinicalStudyParser.loadClinicalStudyFromFile(filename);
        return clinicalStudy.orElse(null);
    }

}
