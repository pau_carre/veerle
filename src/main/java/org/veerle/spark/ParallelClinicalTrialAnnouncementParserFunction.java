package org.veerle.spark;

import java.io.Serializable;
import java.util.Optional;

import org.apache.spark.api.java.function.Function;
import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.jaxb.ClinicalStudy;
import org.veerle.nlp.CriteriaParser;

/**
 * 
 * Spark parallel {@link Function} to parse {@link ClinicalTrialAnnouncement} from  {@link ClinicalStudy}s
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ParallelClinicalTrialAnnouncementParserFunction implements
    Function<ClinicalStudy, ClinicalTrialAnnouncement>, Serializable {

    private static final long serialVersionUID = -8890489222735888795L;

    @Override
    public ClinicalTrialAnnouncement call(ClinicalStudy clinicalStudy) {
        if(clinicalStudy == null){
            return null;
        }
        CriteriaParser criteriaParser = new CriteriaParser();
        Optional<ClinicalTrialAnnouncement> optionalCriteria =
            criteriaParser.parseCriteria(clinicalStudy);
        return optionalCriteria.orElse(null);
    }


}
