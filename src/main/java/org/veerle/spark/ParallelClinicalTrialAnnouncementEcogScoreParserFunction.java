package org.veerle.spark;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.function.Function;
import org.veerle.model.ClinicalTrialAnnouncement;
import org.veerle.model.EcogScore;
import org.veerle.nlp.ExplicitEcogParser;

import scala.Tuple2;

import com.google.common.collect.Ordering;

/**
 * 
 * Spark parallel {@link Function} to extract ECOG scores from {@link ClinicalTrialAnnouncement}s
 * 
 * @author Pau Carre Cardona (pau.carre@gmail.com)
 *
 */
public class ParallelClinicalTrialAnnouncementEcogScoreParserFunction implements 
    Function<ClinicalTrialAnnouncement, Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>>,
    Serializable {
    
    private static final Logger logger = LogManager.getLogger(ParallelClinicalTrialAnnouncementEcogScoreParserFunction.class);
    
    private static final long serialVersionUID = -8890489222735888795L;

    @Override
    public Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>> call(ClinicalTrialAnnouncement clinicalTrialAnnouncement) {
        if(clinicalTrialAnnouncement == null){
            logger.info("-----------> NULL CTA found.");
            return new Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>(clinicalTrialAnnouncement, new ArrayList<>());
        }
        logger.info("-----------> Finding ECOGs for CTA: " + clinicalTrialAnnouncement);
        ExplicitEcogParser ecogParser = new ExplicitEcogParser();
        Set<EcogScore> ecogScores = ecogParser.detectExplicitEcog(clinicalTrialAnnouncement);
        Collection<EcogScore> ecogScoresSorted = Ordering.natural().immutableSortedCopy(ecogScores);
        return new Tuple2<ClinicalTrialAnnouncement, Collection<EcogScore>>(clinicalTrialAnnouncement,
            new ArrayList<EcogScore>(ecogScoresSorted));
    }
    
    

}
